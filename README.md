# Data Intelligence Frontend Task

Our objective is to provide the user with an interface where they can perform their competitor analysis based on the given JSON file, which appears to be a mock from the backend. You are expected to create a proper graph using the information in the file ([response.json](./src/assets/response.json)), and provide the user with the ability to filter by shop, product, and date. (Please see the [image](./src/assets/example-mockup.png) for an example result) 

Feel free to use any CSS approach, external libraries, or Vue.js API of your choice, and add your own creative touches to complete the task. Please use the provided repository as a starting point and add your commits afterwards.


Bonus points for:

- Unit testing and coverage
- Usage of TypeScript


To run the project:
```bash
npm ci
npm run dev
```

On any further question, do not hesitate to reach [me](mailto:kaan.goekdemir@channelpilot.com) via e-mail.